Instalar nodejs: https://nodejs.org/en/download/

Verificar se foi instalado:
node -v
npm -v

Instalar gulp globalmente: npm install -g gulp

Instale gulp localmente no projeto: npm install gulp

Adicione abaixo da linha no seu package.json: "scripts": { "gulp": "gulp" }

Execute gulp no terminal: npm run gulp

no terminal, dentro da pasta, rodar o comando: gulp
accessar no navegador http://localhost:7000/
